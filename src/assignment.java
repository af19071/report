import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class assignment {
    private JPanel root;
    private JLabel topLabel;
    private JButton misoRamenButton;
    private JButton syoyuRamenButton;
    private JButton tonkotsuRamenButton;
    private JButton tsukemenButton;
    private JButton sioRamenButton;
    private JButton yakisobaButton;
    private JButton checkOutButton;
    private JLabel totalLabel;
    private JTextPane textPane1;

    int total =0;

    public static void main(String[] args) {
        JFrame frame = new JFrame("assignment");
        frame.setContentPane(new assignment().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }

    void order(String food, int value) {
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if (confirmation == 0) {

            String currentText = textPane1.getText();
            textPane1.setText(currentText + food +" "+ value + " yen\n");
            total += value;
            JOptionPane.showMessageDialog(null, "Thank you for ordering " + food + "! It will be served as soon as possible.");
            totalLabel.setText("Total" + "   " + total + " " + "yen");
        }
    }


    public assignment() {
        misoRamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Miso Ramen", 200);
            }
        });
        misoRamenButton.setIcon(new ImageIcon(this.getClass().getResource("misoramen.jpg")));

        syoyuRamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Syoyu Ramen", 250);
            }
        });
        syoyuRamenButton.setIcon(new ImageIcon(this.getClass().getResource("syoyuramen.jpg")));

        tonkotsuRamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tonkotsu Ramen", 300);
            }
        });
        tonkotsuRamenButton.setIcon(new ImageIcon(this.getClass().getResource("tonkotsuramen.jpg")));

        tsukemenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tsukemen", 450);
            }
        });
        tsukemenButton.setIcon(new ImageIcon(this.getClass().getResource("tukemen.jpg")));

        sioRamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sio Ramen", 150);
            }
        });
        sioRamenButton.setIcon(new ImageIcon(this.getClass().getResource("sioramen.jpg")));

        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba", 500);
            }
        });
        yakisobaButton.setIcon(new ImageIcon(this.getClass().getResource("sioramen.jpg")));

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    int receipt = JOptionPane.showConfirmDialog(
                            null,
                            "Would you like  a receipt?",
                            "Message",
                            JOptionPane.YES_NO_OPTION);
                    JOptionPane.showMessageDialog(null,
                            "Thank you for using this store!\nThe total price is "
                                    + total
                                    + " yen.\n");
                    total = 0;
                    totalLabel.setText("Total" + "   " + total + " " + "yen");
                    textPane1.setText(" ");
                }
            }
        });
    }

}

